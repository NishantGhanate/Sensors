package com.example.nishant.sensors;


import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;


public class MainActivity extends AppCompatActivity  {
    SensorManager sensorManager;
    Sensor proximitySensor,accelometer , gyroscope;
    SensorEventListener AccelometerListener ,GyroscopeListener;
    TextView textView1,textView2,textViewBelow;
    ToggleButton toggleButton1 ,toggleButton2;
    ConstraintLayout constraintLayout;
    String s;
    Float decimalValue;
    float[] acc , gyro;
    private static final String TAG = "Sensor values";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView1 = findViewById(R.id.textView1);
        textView2 = findViewById(R.id.textView2);
        textViewBelow = findViewById(R.id.textViewBelow);
        toggleButton1 = findViewById(R.id.toggleButton1);
        toggleButton2 = findViewById(R.id.toggleButton2);
        constraintLayout = findViewById(R.id.Layout);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        if (sensorManager != null) {
            accelometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
            if(gyroscope == null){
                Toast.makeText(this, " Sorry you device does not have gyroscope sensor", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        //color = Color.argb(255, x, y, y);
        //constraintLayout.setBackgroundColor(color);

        toggleButton1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Toast.makeText(MainActivity.this, " ON ", Toast.LENGTH_SHORT).show();
                    Accelometer();
                    sensorManager.registerListener(AccelometerListener,accelometer,SensorManager.SENSOR_DELAY_NORMAL);
                } else{
                    Toast.makeText(MainActivity.this, " OFF  ", Toast.LENGTH_SHORT).show();
                    sensorManager.unregisterListener(AccelometerListener);
                }
            }
        });

        toggleButton2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Gyroscope();
                    sensorManager.registerListener(GyroscopeListener, gyroscope,SensorManager.SENSOR_DELAY_NORMAL);
                }else{
                    Toast.makeText(MainActivity.this, " OFF  ", Toast.LENGTH_SHORT).show();
                    sensorManager.unregisterListener(GyroscopeListener);
                }
            }
        });



    }// End onCreate

    private void Accelometer(){

        AccelometerListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                acc = event.values;
                Log.d("Acc",acc[0]+ "," +  acc[1] + "," + acc[2] );
                textView1.setText(String.valueOf(acc[0]) + "\n" + String.valueOf(acc[1]) + "\n" + String.valueOf(acc[2])); // Not the best way
                s = event.values.toString();
                textViewBelow.setText( s );
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
    }

    private void Gyroscope(){
        GyroscopeListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                gyro = event.values;
                Log.d("Gyroscope", gyro[0] + " ," + gyro[1] + " ," + gyro[2]);
                textView2.setText(String.valueOf(gyro[0]) + "\n" +String.valueOf(gyro[1]) + "\n" + String.valueOf(gyro[2]) );
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
    }


    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(AccelometerListener);
        sensorManager.unregisterListener(GyroscopeListener);
    }
}
